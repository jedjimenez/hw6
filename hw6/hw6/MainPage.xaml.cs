﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Connectivity;
using System.Net.Http;
using Newtonsoft.Json;
using QuickType;

namespace hw6
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            
        }
        
        public async void GetDefinition(object sender, EventArgs e)
        {
            if (CrossConnectivity.Current.IsConnected) //check if there is internet connection and do the following
            {
                var client = new HttpClient(); //creates client to make HTTP request
                var uri = new Uri(string.Format($"https://owlbot.info/api/v2/dictionary/" + wordEntry.Text));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                HttpResponseMessage response = await client.SendAsync(request); //send an HTTP request
                Definition[] words = null; //initialize an object where the HTTP content will be stored

                if (response.IsSuccessStatusCode) //check if HTTP request succeeded or failed
                {
                    var content = await response.Content.ReadAsStringAsync(); //writes the HTTP content into a string
                    words = Definition.FromJson(content); //deserialize 
                }
                else //if failed show the following alert
                {
                    await base.DisplayAlert("Word not found", "Please enter a different word.", "OK");
                }

                listView.ItemsSource = words; //add data to the listview from the object array

            }
            else //show an alert if there is no internet connection 
            {
                await base.DisplayAlert("Error", "Please Check Your Internet Connection", "OK");   
            }
        }
    }
}
